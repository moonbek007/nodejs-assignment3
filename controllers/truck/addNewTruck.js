import { ERROR_MESSAGE } from "../../utils/constants";
import jwt from "jsonwebtoken";
import TruckModel from "../../models/TruckModel";
import chalk from "chalk";

const addNewTruck = (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  const { type } = req.body;
  if (!type || !token) {
    console.log(chalk.red("Insufficient body params or no token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else if (
    type !== "SPRINTER" &&
    type !== "SMALL STRAIGHT" &&
    type !== "LARGE STRAIGHT"
  ) {
    console.log(chalk.red("Invalid truck type!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    if (myData.role !== "DRIVER") {
      console.log(chalk.red("Unauthorized"));
      res.status(400).json(ERROR_MESSAGE);
    } else {
      TruckModel.create(
        {
          created_by: myData._id,
          assigned_to: null,
          type,
          status: "IS",
          created_date: new Date(),
        },
        (err) => {
          if (err) {
            res.status(500).json(ERROR_MESSAGE);
          } else {
            console.log(
              chalk.green(
                `Created a new truck of type '${type}' for driver ${myData.email}`
              )
            );
            res.status(200).json({
              message: "Truck created successfully",
            });
          }
        }
      );
    }
  }
};

export default addNewTruck;
