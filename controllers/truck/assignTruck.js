import { ERROR_MESSAGE } from "../../utils/constants";
import TruckModel from "../../models/TruckModel";
import chalk from "chalk";
import jwt from "jsonwebtoken";

const assignTruck = (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    TruckModel.findOne(
      {
        created_by: myData._id,
        assigned_to: myData._id,
        status: "OL",
      },
      (err, truckOnLoad) => {
        if (err) console.log(err);
        else if (truckOnLoad) {
          console.log(
            chalk.red("Can't assign a new truck when a driver is ON LOAD")
          );
          res.status(400).json(ERROR_MESSAGE);
        } else {
          TruckModel.findOneAndUpdate(
            {
              created_by: myData._id,
              assigned_to: myData._id,
              status: "IS",
            },
            { assigned_to: null },
            (err) => {
              if (err) {
                res.status(500).json(ERROR_MESSAGE);
              } else {
                TruckModel.findOneAndUpdate(
                  {
                    _id: id,
                    created_by: myData._id,
                  },
                  { assigned_to: myData._id },
                  { new: true },
                  (err, truck) => {
                    if (err) res.status(500).json(ERROR_MESSAGE);
                    else {
                      if (!truck) {
                        console.log(
                          chalk.red(
                            `No truck with id '${id}' found or Unauthorized!`
                          )
                        );
                        res.status(400).json(ERROR_MESSAGE);
                      } else {
                        console.log(
                          chalk.green(
                            `Truck '${id}' assigned to ${myData.email}`
                          )
                        );
                        res.status(200).json({
                          message: "Truck assigned successfully",
                        });
                      }
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  }
};

export default assignTruck;
