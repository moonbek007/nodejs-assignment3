import { ERROR_MESSAGE } from "../../utils/constants";
import jwt from "jsonwebtoken";
import TruckModel from "../../models/TruckModel";
import chalk from "chalk";

const getAllTrucks = (req, res) => {
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    if (myData.role !== "DRIVER") {
      console.log(chalk.red("Unauthorized"));
      res.status(400).json(ERROR_MESSAGE);
    } else {
      TruckModel.find({ created_by: myData._id }, (err, trucks) => {
        if (err) res.status(500).json(ERROR_MESSAGE);
        else {
          // if (trucks.length === 0) {
          //   console.log(chalk.red("No trucks found."));
          //   res.status(400).json(ERROR_MESSAGE);
          // } else {
          res.status(200).json({
            trucks: trucks.map((truck) => {
              return {
                _id: truck._id,
                created_by: truck.created_by,
                assigned_to: truck.assigned_to,
                type: truck.type,
                status: truck.status,
                created_date: truck.created_date,
              };
            }),
          });
          // }
        }
      });
    }
  }
};

export default getAllTrucks;
