import { ERROR_MESSAGE } from "../../utils/constants";
import TruckModel from "../../models/TruckModel";
import chalk from "chalk";
import jwt from "jsonwebtoken";

const findTruck = async (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    TruckModel.findOne(
      {
        _id: id,
        created_by: myData._id,
      },
      (err, truck) => {
        if (err) {
          res.status(500).json(ERROR_MESSAGE);
        } else {
          if (!truck) {
            console.log(
              chalk.red(`No truck with id '${id}' was found or Unauthorized!`)
            );
            res.status(400).json(ERROR_MESSAGE);
          } else {
            const { _id, created_by, assigned_to, type, status, created_date } =
              truck;
            res.status(200).json({
              truck: {
                _id,
                created_by,
                assigned_to,
                type,
                status,
                created_date,
              },
            });
          }
        }
      }
    );
  }
};

export default findTruck;
