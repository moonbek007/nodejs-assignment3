import { ERROR_MESSAGE } from "../../utils/constants";
import jwt from "jsonwebtoken";
import UserModel from "../../models/UserModel";
import bcrypt from "bcryptjs";
import chalk from "chalk";

const updateMyPassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const [, token] = req.headers["authorization"].split(" ");
  if (!oldPassword || !newPassword || !token) {
    console.log(chalk.red("Insufficient credentials or no token received !"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    UserModel.findOne(
      { email: myData.email },
      // { password: newPasswordHashed },
      // { new: true },
      async (err, user) => {
        if (err) res.status(500).json(ERROR_MESSAGE);
        else {
          if (!user) {
            console.log(chalk.red("Invalid token!"));
            res.status(400).json(ERROR_MESSAGE);
          } else {
            const hasCorrectPassword = await bcrypt.compare(
              oldPassword,
              user.password
            );
            if (!hasCorrectPassword) {
              console.log(chalk.red("Passwords don't match!"));
              res.status(400).json(ERROR_MESSAGE);
            } else {
              const newPasswordHashed = await bcrypt.hash(newPassword, 10);
              user.password = newPasswordHashed;
              user.save((err) => {
                if (err) res.status(500).json(ERROR_MESSAGE);
                else {
                  console.log(
                    chalk.green(`Changed password for ${user.email}`)
                  );
                  res.status(200).json({
                    message: "Password changed successfully",
                  });
                }
              });
            }
          }
        }
      }
    );
  }
};

export default updateMyPassword;
