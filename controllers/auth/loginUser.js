import UserModel from "../../models/UserModel";
import { ERROR_MESSAGE } from "../../utils/constants";
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import chalk from "chalk";

const loginUser = (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(400).json(ERROR_MESSAGE);
    console.log(chalk.red("Insufficient credentials!"));
  } else {
    UserModel.findOne({ email }, async (err, user) => {
      if (err) {
        res.status(500).json(ERROR_MESSAGE);
      } else {
        if (user == null || !(await bcrypt.compare(password, user.password))) {
          res.status(400).json(ERROR_MESSAGE);
          console.log(chalk.red("Invalid email or password!"));
        } else {
          const token = jwt.sign(
            {
              email,
              _id: user._id,
              created_date: user.created_date,
              role: user.role,
            },
            "secret"
          );
          console.log(chalk.green(`User ${email} logged in.`));
          res.status(200).json({ jwt_token: token });
        }
      }
    });
  }
};

export default loginUser;
