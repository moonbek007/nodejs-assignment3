import { ERROR_MESSAGE } from "../../utils/constants";
import LoadModel from "../../models/LoadModel";
import chalk from "chalk";
import jwt from "jsonwebtoken";

const findLoad = async (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    LoadModel.findOne(
      {
        _id: id,
      },
      (err, load) => {
        if (err) {
          res.status(500).json(ERROR_MESSAGE);
        } else {
          if (!load) {
            console.log(chalk.red(`No load with id '${id}' was found!`));
            res.status(400).json(ERROR_MESSAGE);
          } else if (
            myData._id !== load.created_by &&
            myData._id !== load.assigned_to
          ) {
            console.log(chalk.red("Unauthorized!"));
            res.status(400).json(ERROR_MESSAGE);
          } else {
            res.status(200).json({
              load: {
                _id: load._id,
                created_by: load.created_by,
                assigned_to: load.assigned_to,
                status: load.status,
                state: load.state,
                name: load.name,
                payload: load.payload,
                pickup_address: load.pickup_address,
                delivery_address: load.delivery_address,
                dimensions: load.dimensions,
                logs: load.logs,
                created_date: load.created_date,
              },
            });
          }
        }
      }
    );
  }
};

export default findLoad;
