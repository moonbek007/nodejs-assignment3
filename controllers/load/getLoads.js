import { ERROR_MESSAGE } from "../../utils/constants";
import LoadModel from "../../models/LoadModel";
import chalk from "chalk";
import jwt from "jsonwebtoken";

const getLoads = async (req, res) => {
  const status = req.query.status;
  const limit =
    !req.query.limit || req.query.limit < 0
      ? 10
      : req.query.limit > 50
      ? 50
      : req.query.limit;
  const offset =
    !req.query.offset || req.query.offset < 0 ? 0 : req.query.offset;
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    console.log(chalk.red("No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else if (!status) {
    const myData = jwt.verify(token, "secret");
    if (myData.role === "DRIVER") {
      let response = [];
      LoadModel.find(
        { assigned_to: myData._id, status: "ASSIGNED" },
        (err, loads) => {
          if (err) res.status(500).json(ERROR_MESSAGE);
          else {
            response = [...loads];
            LoadModel.find(
              { assigned_to: myData._id, status: "SHIPPED" },
              (err, loads) => {
                if (err) res.status(500).json(ERROR_MESSAGE);
                else {
                  response = [...response, ...loads];
                  if (response.length !== 0) {
                    res.status(200).json({
                      loads: response
                        .slice(offset)
                        .slice(0, limit)
                        .map((load) => {
                          return {
                            _id: load._id,
                            created_by: load.created_by,
                            assigned_to: load.assigned_to,
                            status: load.status,
                            state: load.state,
                            name: load.name,
                            payload: load.payload,
                            pickup_address: load.pickup_address,
                            delivery_address: load.delivery_address,
                            dimensions: load.dimensions,
                            logs: load.logs,
                            created_date: load.created_date,
                          };
                        }),
                    });
                  } else {
                    res.status(200).json({
                      loads: [],
                    });
                  }
                }
              }
            );
          }
        }
      );
    } else {
      LoadModel.find({ created_by: myData._id }, (err, loads) => {
        if (err) res.status(500).json(ERROR_MESSAGE);
        else {
          if (loads.length !== 0) {
            res.status(200).json({
              loads: loads
                .slice(offset)
                .slice(0, limit)
                .map((load) => {
                  return {
                    _id: load._id,
                    created_by: load.created_by,
                    assigned_to: load.assigned_to,
                    status: load.status,
                    state: load.state,
                    name: load.name,
                    payload: load.payload,
                    pickup_address: load.pickup_address,
                    delivery_address: load.delivery_address,
                    dimensions: load.dimensions,
                    logs: load.logs,
                    created_date: load.created_date,
                  };
                }),
            });
          } else {
            res.status(200).json({
              loads: [],
            });
          }
          // res.status(200).json({
          //   loads: loads.slice(offset).slice(0, limit),
          // });
        }
      });
    }
  } else {
    const myData = jwt.verify(token, "secret");
    if (myData.role === "DRIVER") {
      LoadModel.find(
        { assigned_to: myData._id, status: status },
        (err, loads) => {
          if (err) res.status(500).json(ERROR_MESSAGE);
          else {
            // if (!loads.length) {
            //   console.log(chalk.red(`No loads found or Invalid token!`));
            //   res.status(400).json(ERROR_MESSAGE);
            // }
            //  else {
            if (loads.length !== 0) {
              res.status(200).json({
                loads: loads
                  .slice(offset)
                  .slice(0, limit)
                  .map((load) => {
                    return {
                      _id: load._id,
                      created_by: load.created_by,
                      assigned_to: load.assigned_to,
                      status: load.status,
                      state: load.state,
                      name: load.name,
                      payload: load.payload,
                      pickup_address: load.pickup_address,
                      delivery_address: load.delivery_address,
                      dimensions: load.dimensions,
                      logs: load.logs,
                      created_date: load.created_date,
                    };
                  }),
              });
            } else {
              res.status(200).json({
                loads: [],
              });
            }
            // res.status(200).json({
            //   loads: loads.slice(offset).slice(0, limit),
            // });
            // }
          }
        }
      );
    } else {
      LoadModel.find(
        {
          created_by: myData._id,
          status: status,
        },
        (err, loads) => {
          if (err) {
            res.status(500).json(ERROR_MESSAGE);
          } else {
            // if (!loads.length) {
            //   console.log(chalk.red(`No loads found or Invalid token!`));
            // res.status(400).json(ERROR_MESSAGE);
            // } else {
            if (loads.length !== 0) {
              res.status(200).json({
                loads: loads
                  .slice(offset)
                  .slice(0, limit)
                  .map((load) => {
                    return {
                      _id: load._id,
                      created_by: load.created_by,
                      assigned_to: load.assigned_to,
                      status: load.status,
                      state: load.state,
                      name: load.name,
                      payload: load.payload,
                      pickup_address: load.pickup_address,
                      delivery_address: load.delivery_address,
                      dimensions: load.dimensions,
                      logs: load.logs,
                      created_date: load.created_date,
                    };
                  }),
              });
            } else {
              res.status(200).json({
                loads: [],
              });
            }
            // res.status(200).json({
            //   loads: loads.slice(offset).slice(0, limit),
            // });
            // }
          }
        }
      );
    }
  }
};

export default getLoads;
