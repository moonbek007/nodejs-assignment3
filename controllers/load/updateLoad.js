import { ERROR_MESSAGE } from "../../utils/constants";
import LoadModel from "../../models/LoadModel";
import chalk from "chalk";
import jwt from "jsonwebtoken";

const updateLoad = (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers["authorization"].split(" ");
  const fields = [
    "name",
    "payload",
    "pickup_address",
    "delivery_address",
    "dimensions",
  ];
  const dimensions = ["width", "length", "height"];
  let hasAllFields = true;
  fields.forEach((field) => {
    if (!req.body[field]) {
      hasAllFields = false;
    } else if (field === "dimensions" && req.body[field]) {
      dimensions.forEach((dimension) => {
        if (!req.body[field][dimension]) {
          hasAllFields = false;
        }
      });
    }
  });
  if (!hasAllFields || !token) {
    console.log(chalk.red("Insufficient body params or No token received!"));
    res.status(400).json(ERROR_MESSAGE);
  } else {
    const myData = jwt.verify(token, "secret");
    if (myData.role !== "SHIPPER") {
      console.log(chalk.red("Unauthorized!"));
      res.status(400).json(ERROR_MESSAGE);
    } else {
      LoadModel.findOneAndUpdate(
        {
          _id: id,
          created_by: myData._id,
          status: "NEW",
        },
        {
          name: req.body.name,
          payload: req.body.payload,
          pickup_address: req.body.pickup_address,
          delivery_address: req.body.delivery_address,
          dimensions: req.body.dimensions,
        },
        { new: true },
        (err, load) => {
          if (err) {
            res.status(500).json(ERROR_MESSAGE);
          } else {
            if (!load) {
              console.log(
                chalk.red(`No load with id '${id}' found or Unauthorized!`)
              );
              res.status(400).json(ERROR_MESSAGE);
            } else {
              console.log(chalk.green(`Updating load with id '${id}'`));
              res.status(200).json({
                message: "Load details changed successfully",
              });
            }
          }
        }
      );
    }
  }
};

export default updateLoad;
