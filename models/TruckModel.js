import mongoose from "mongoose";

const TruckSchema = mongoose.Schema({
  created_by: String,
  assigned_to: String,
  type: String,
  status: String,
  created_date: Date,
});

export default mongoose.model("TruckModel", TruckSchema);
