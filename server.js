import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import registerUser from "./controllers/auth/registerUser";
import loginUser from "./controllers/auth/loginUser";
import getMyInfo from "./controllers/user/getMyInfo";
import deleteMyInfo from "./controllers/user/deleteMyInfo";
import updateMyPassword from "./controllers/user/updateMyPassword";
import getAllTrucks from "./controllers/truck/getAllTrucks";
import addNewTruck from "./controllers/truck/addNewTruck";
import findTruck from "./controllers/truck/findTruck";
import updateTruck from "./controllers/truck/updateTruck";
import removeTruck from "./controllers/truck/removeTruck";
import assignTruck from "./controllers/truck/assignTruck";
import addNewLoad from "./controllers/load/addNewLoad";
import getActiveLoad from "./controllers/load/getActiveLoad";
import changeLoadState from "./controllers/load/changeLoadState";
import findLoad from "./controllers/load/findLoad";
import updateLoad from "./controllers/load/updateLoad";
import deleteLoad from "./controllers/load/deleteLoad";
import postLoad from "./controllers/load/postLoad";
import getShipmentInfo from "./controllers/load/getShipmentInfo";
import getLoads from "./controllers/load/getLoads";

require("dotenv").config();

const URI = `mongodb+srv://${process.env.dbLOGIN}:${process.env.dbPASSWORD}@assignment3.2ntag.mongodb.net/finalProject?retryWrites=true&w=majority`;
mongoose.connect(
  URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    autoIndex: true,
  },
  (err) => {
    if (err) {
      throw new Error(err);
    }
    console.log("connected to mongoDB");
  }
);

const app = express();
const PORT = process.env.PORT || 8080;

app.use(express.json());
app.use(express.static("./build"));
app.use(cors());
app.use((req, res, next) => {
  console.log(req.method, req.originalUrl);
  next();
});

// app.get("/", (req, res) => {
//   res
//     .status(200)
//     .send("<h1 style='text-align:center'>Node.js Assignment 1</h1>");
// });

app.route("/api/users/me").get(getMyInfo).delete(deleteMyInfo);
app.route("/api/users/me/password").patch(updateMyPassword);

app.route("/api/trucks").get(getAllTrucks).post(addNewTruck);
app
  .route("/api/trucks/:id")
  .get(findTruck)
  .put(updateTruck)
  .delete(removeTruck);
app.route("/api/trucks/:id/assign").post(assignTruck);

app.route("/api/loads").get(getLoads).post(addNewLoad);
app.route("/api/loads/active").get(getActiveLoad);
app.route("/api/loads/active/state").patch(changeLoadState);
app.route("/api/loads/:id").get(findLoad).put(updateLoad).delete(deleteLoad);
app.route("/api/loads/:id/post").post(postLoad);
app.route("/api/loads/:id/shipping_info").get(getShipmentInfo);

app.route("/api/auth/register").post(registerUser);
app.route("/api/auth/login").post(loginUser);

app.listen(PORT, () => {
  console.log(`Starting the server on port ${PORT}`);
});
