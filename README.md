# Nodejs Assignment3

## To run this app , clone the project into a local repository , go to newly created directory and run a command 'npm start' .

### This project is build using Express framework for setting up api endpoints , Joi npm package to verify all the required body and query

### parameters are provided by a client , and some dev devependencies to like babel-cli to make a better delevoper experience . The frontend

### of the project is created using React and its ecosystems such as React Router , ContextAPI for state management and so on .
